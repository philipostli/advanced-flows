# Advanced Flows Collection

Her vil jeg dele en samling av flows som kan kjøres på alle Homey i skyen.
Formålet er å løse problemet til ikke-Pro brukere der apper er begrenset til kun fysiske enheter og har ikke etterspurt funksjonalitet som prisstyring, tidtaking, gruppering osv. Dette kan løses ved å kalle direkte på API med innebygget funksjonalitet for å sende HTTP-requests  som finnes i logikk-kortene. Jeg deler også løsninger for produkter der produsenten enda ikke har laget en Homey app, men der åpent API kan benyttes.

OBS!  
Vær oppmerksom på at innloggingsdetaljer som e-post eller passord lagres i flowen (på din Homey konto) og er ikke nødvendigvis lagret med tanke på sikring mot innbrudd.   
Løsningene benyttes derfor på eget ansvar. 

## Samling
- ![logo](strømpris-planlagt-start/logo-strompris.png) [Strømpris basert styring](strømpris-planlagt-start/README.md) 
- ![logo](mill-overstyre-program/logo-mill.png) [Mill varmeovn gen 3](mill-overstyre-program/README.md) 