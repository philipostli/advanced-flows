# Planlegg oppstart basert på strømpris

- [Tilbake til samling](../README.md)

Dette er en rekke flows som muliggjør strømprisbasert oppstart av et apparat på Homey (ikke Pro) som ikke har tilgang til scripts eller apper som har denne funksjonaliteten innebygget.
Siden Homey kun støtter apper fra produsenter av smartutstyr, er man avhengig av at de også bygger inn strømpris styring med egne flow-kort i appen sin, og jeg har tilgode å finne en slik app. 
En mulighet er Tibber-appen, som fungerer med "Pulse" enheten dems. Men pris-kortene er kun tilgjengelige dersom man samtidig er strømkunde. Derfor benytter jeg et gratis API som egentlig er en nettside med dagens priser time for time: www.hvakosterstrommen.no

Det tar noe tid å sette opp, men fungerer utmerket.

For å benytte dette med andre apparater enn i mitt eksempel en oppvaskmaskin, så behøver man kun endre disse plassene:

1. Trigger i første flowen <font color="#09E058">**Planlegg oppstart**</font>
2. Når-kortene som starter maskinen i flowen  <font color="#09E058">**Oppvaskmaskin planlagt start**</font>

Også er det en del valgfrie komponenter som med fordel kan utelates, som feilmeldinger eller justerting av dag-variabelen basert på klokkeslett.  
Men hovedprinsippene er illustrert i første flowen og demonstrer at dette er mulig også på en Homey Bridge.
Flow som henter strømpris kan brukes separat til andre flows.

## Planlegg oppstart
![Flow planlegg oppstart](flow-planlegg.png)

1.) Flow trigges av at apparat blir skrudd på <font color="white">**Oppvaskmaskin**</font> - <font color="#AA09E0">**Slått på**</font>.  
2.) For å ikke trigges når apparat startes på den planlagte tiden legger vi til <font color="white">**Oppvaskmaskin**</font> - <font color="#AA09E0">**Kontaktalarmen er på**</font> som betyr at døren er åpnet. Og det er den kun når vi trykker på knappen for å starte oppvaskmaskinen.    
3.) Kaller på flow <font color="#09E058">**Oppdater dato**</font> for å sette variabler for dag, måned og år som brukes i URL som henter strømprisene. (eks. 2023/12-09)  
4.-8.) Dersom klokken er før 12 hentes prisene for dagens dato. Ellers legger vi til 1 dag på dag-variabelen. Pass på 0 foran en sifrede tall. Dette kan med fordel legges in i flowen <font color="#09E058">**Oppdater dato**</font>.  
9.) Kaller flow <font color="#09E058">**Hent lavest strømpris**</font> som setter en tallvariabel (<font color="#2B95E1">**Klokka lavest pris**</font>) for den timen som har lavest pris.  
10.) Sender push-varsling som spør om svar på om maskinen skal starte på angitt klokkeslett.  
11.) Flowen <font color="#09E058">**Oppvaskmaskin planlagt start**</font> aktiveres. Dette er en flow som kjører hver time og sjekker om maskinen skal starte da eller ikke.
12.) Variabel <font color="#2B95E1">**Oppvaskmaskin starter klokka**</font> blir satt til klokkeslettet for lavest pris. (Disse to variablene kan godt være den samme for enkelhets skyld)  
13.) Når oppvaskmaskinen har startet et program, deaktiverer vi flowen igjen, slik at den kun kjører en gang, frem til neste gang trykker på knappen og planlegger en ny vask. Neste dag kan laveste pris være på et annet klokkeslett.

## Oppdater dato
![Flow oppdater dato](flow-oppdater-dato.png)

Flowen oppdaterer tre varabler <font color="#2B95E1">**Dag**</font> (tekst-variabel), <font color="#2B95E1">**Måned**</font> (tekst-variabel), <font color="#2B95E1">**År**</font> (tall-variabel).

Denne metoden er tungvinn, men jeg har tilgode og finne en enklere måte å hente ut dagen separert på. Det finnes en tag for dagens dato på formen '31-12-2023'. Dersom det er mulig å manipulere denne med en støttet funksjon slik som {{min()}} og + operatoren (Jeg har ikke fått substr() til å fungere), kan en alternativ flow se mye enklere ut:

![Flow oppdater dato alternativ](flow-oppdater-dato-alt.png)

## Hent lavest størmpris

![Flow hent lavest strømpris](flow-hent-lavest-strømpris.png)

Denne flowen kan se skummel ut, men handler mest om å få tak i Resultat-tagene fra "Analyser JSON"-kortene som kan være tricky (ettersom alle tag-ene heter det samme).
Trikset her er å dele opp i blokker på 4 og 4 og sammenligne de hver for seg for til slutt og sitte igjen med den laveste prisen.
For hver min() sammenligning, tegn opp *en og en* linje fra analyse-kortene. Da vil det være den bakerste av resultat-tagene:

![Eksempel på resultat-tager](resultat-tager.png)

Først sendes en GET-request til nettsiden www.hvakosterstrommen.no med dagens dato fra variablene som vi beregnet tidligere. (Eller morgendagen dersom det er justert).
Denne fungerer for østlandet (strømprissone NO1). Dersom du bor i en annen landsdel må denne byttes ut med riktig kode. Kodene kan du finne på nettsiden.
Variabelen <font color="#2B95E1">**Lavest pris**</font> settes til resultatet av min() funksjonen som er kjørt på alle de 24 timene.
Den laveste prisen må så sjekkes opp mot de ulike prisene for å avgjøre hvilken av de 24 timene den tilhører.
Om den er lik resultatet fra det første analyse-kortet, vet vi at det er kl 00. Etc. Kortene er sortert på timene i stigende rekkefølge.

## Oppvaskmaskin planlagt start

![Flow oppvaskmaskin planlagt start](flow-oppvaskmaskin-planlagt-start.png)

Denne er ganske rett frem. For hver time (Når-kort) trigges denne (så lenge den er aktivert). Vi sjekker om variabelen med klokkeslettet for den laveste prisen er lik nåværende time. I så fall skru på maskinen, vent 5 sekunder til den er klar, og start valgfritt program. Deaktiver så seg selv (dette gjøres også i hovedflowen over). Forskjellen er at i hovedflowen deaktiveres denne dersom maskinen blir startet manuelt, for å unngå at den kjører den planlagte oppstarten senere samme dag. Men i selve flowen som starter etter planen (denne flow) så deaktiveres den etter den selv har kjørt etter planen. Den siste kan i grunn droppes ettersom den første deaktiverer planlagt oppstart i alle tilfeller.

Så er det en fiffig løsning for å feilmelde dersom maskinen skulle startet men døren er åpnet, eller dersom den skal starte på et senere tidspunkt og døren er åpnet, eller dersom programmet ikke vil starte.