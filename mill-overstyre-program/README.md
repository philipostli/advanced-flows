# Mill gen 3 API

- [Tilbake til samling](../README.md)

Dette prosjektet viser hvordan en kan styre Mill generasjon 3 ovner *uten* tredjeparts apper. 
Ved hjelp av de innebyggede logikk-kortene i Advanced Flow kan vi styre det offisielle API'et som er [dokumetert her](http://mn-be-prod-documentation.s3-website.eu-central-1.amazonaws.com/#/).
Siden flowen er tiltenkt Homey kjørende i skyen, benytter vi Mill sitt sentrale API over internett og ikke lokalt API som er mulig å bruke dersom enheten som kjører flowen er på samme nettverk som ovnen, t.eks et script kjørt på en PC eller en Homey Pro 2023.

Prosjektet krever at ovnen som skal styres er koblet til WiFi og er lagt til i et rom i Mill appen. 
For å gjøre det kreves en bruker som er logget inn i appen. Vi trenger både brukeranvn, passord og id til rommet vi skal styre.

Målet med [flowen](#overstyre-rom-program) er å overstyre ovnens planlagte program eller modus, basert på tilstedeværesle eller søvn. Følgende beskriver metoden steg for steg.

## Få tak i {ROOM_ID}
Metoden for å hente ut ID er valgfri, men jeg anbefaler å lagre kallene og kjøre de i [Postman](https://www.postman.com/).

### 1. Logg inn
Først må man logge inn og få tak i en Bearer-token:

``
POST https://api.millnorwaycloud.com/customer/auth/sign-in
``

header:
```
  Content-Type: application/json
```

body:
```json
{  
  'login': {login},  
  'password': {password}  
}  
```

response:
```json
{
  "idToken": "JWT",
  "refreshToken": "JWT"
}
```

Bytt ut {login} med din e-post som du bruker for å logge inn i Mill appen. Bytt ut {passord} med ditt passord.  
Verdien i **{idToken}** trenger vi videre i alle kall til API'et og for enkelhets-skyld legges dette til før alle kall i flow. Mer om det senere.

### 2. Finn {HOUSE_ID]
Så må vi få tak i husets id:

```
GET https://api.millnorwaycloud.com/houses
```

header:
```
Authorization: Bearer {idToken}
```

response:
```json
{
  "ownHouses": [
    {
      "id": "GUID",
      "name": "Hjemme",
      ...
    },
    ...
  ]
}
```

Verdien i **"id"** bruker vi videre som **{HOUSE_ID}**. 
Det er mulig å ha flere hus knyttet til samme bruker, og denne id i kombinasjon med din **{idToken}** styrer din ovn over internett.
Du kan også se hus som er delt med deg under "sharedHouses".

### 3. Liste husets rom

Så henter vi en liste med rom som vi kan styre med Homey:

```
GET https://api.millnorwaycloud.com/houses/{HOUSE_ID}/rooms
```

header:
```
Authorization: Bearer {idToken}
```

response:
```json
{
  "rooms": [
    {
      "id": "GUID",
      "name": "Kontor",
      ...
    },
    {
      "id": "GUID",
      "name": "Soverom",
      ...
    },
  ...
  ]
}
```

Bytt ut {HOUSE_ID} i URL'en med verdien fra forrige steg.
Verdien i **"id"** sparer du i {ROOM_ID} og brukes i følgende flow.

## Overstyre rom-program

![Flow_overstyr_program](flow-overstyr-program.png)

1. Flow trigges av *"Siste person dro hjemmefra"* og *"Den første personen kom hjem"* kortene som er innebygget i appen ***"Tilstedeværelse"***.
2. Videre sendes en POST request tilsvarende den [beskrevet over](#1-logg-inn), for å logge inn og hente en **{id_token}**.
3. Responsen fra server behandles som en JSON og verdien til **idToken** blir lagret i en tag som kalles ***#Resultat***.
4. Dersom login gikk bra OG (ALL) trigger som startet flow er *"Den siste personen dro hjemmefra"* så sender vi en request for å sette permanent modus borte/"away". Legg merke til at ***#Resultat***-tagen representerer token fra login, og {ROOM_ID} settes til den id for rommet vi skal overstyre.
5. Pushmelding sendes (valgfritt) for å sjekke at flowen fungerer.
6. En flow med navn **Debug** kalles med en tekst-verdi. Den logger de ulike stegene i flowen til et Google Regneark, for lettere feilsøking. Se [beskrivelse](#debug) under.
7. Det er lurt å logge ved feil i kallet til API'et. Det er mye som kan gå galt der.
8. Om noen kommer hjem så sendes et DELETE-kall til API for å fjerne den permanente overstyringen som er satt på rommet tidligere.

De ulike modusene som kan settes er *away*, *sleep*, *comfort*, *normal*, *always_heating* og *off*.

Dersom flere ovner skal settes i samme flow kan det legges til parallelle kall etter innlogging. Samme token kan benyttes flere ganger innenfor noen minutters gyldighet.


NB!
Optimalt sett skal man kun sende passordet sitt én gang og videre bruke **refreshToken** for å hente **idToken** på nytt. 
Da slipper man både å risikere å miste passordet, eller at noen får tak i det.
En refreshToken kan enkelt slettes, mens et passord må byttes, eller i verste fall mister man tilgang til sis egen konto.

## Debug
![Debug](debug.png)

En enkel flow som kalles med en tekst-streng som hentes med taggen ***#Startverdi***. Denne kan skrus av dersom variabelen ***#Debug modus*** er lik 'nei'. Et regneark med navn "Debug Homey" er opprettet i Google Dokumenter. Denne appen må legges til i Homey og logges inn på samme konto. En kan da kalle på et kort som logger ***#Start verdi*** -tekststrengen vår og tidspunktet det logges.

